# QuizGPT

This project was created during the Summer 2023 Publicis Sapient Software Engineering internship program in Boston, Massachusetts

## Development Team

### Frontend
*Leila Baniassad and Nolan Gelinas*

### Backend
*Ananya Sharma and Miles Acquaviva*