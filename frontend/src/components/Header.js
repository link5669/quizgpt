import React from 'react';
import '../index.css';

const Header = () => {
  return (
    <header className="header">
      <h1 className="header-title">QuizGPT</h1>
    </header>
  );
};

export default Header;